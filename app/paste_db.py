"""
paste - paste_db.py
Provides an interface to work with the paste database via the PasteDatabase class
By Katelyn Hamer
"""

import sqlite3
from pathlib import Path
from shortuuid import uuid
import logging

logger = logging.basicConfig(level=logging.DEBUG)


class PasteDatabase:
    def __init__(self, db_path: Path):
        self.db_path = Path(db_path)
        if not self.db_path.exists():
            raise FileNotFoundError(
                f"Can't open database {self.db_path}, no such file or directory."
            )

    def init_db(self):
        """Initialise the database"""
        with sqlite3.connect(self.db_path) as cursor:
            cursor.execute(
                (
                    'CREATE TABLE "pastes" ('
                    '"id" TEXT NOT NULL UNIQUE,'
                    '"author" TEXT NOT NULL,'
                    '"content" TEXT NOT NULL,'
                    '"filename" TEXT NOT NULL,'
                    '"public" INT NOT NULL,'
                    'PRIMARY KEY("id"))'
                )
            )

    def get_paste(self, paste_id: str) -> dict:
        """Get a paste, returns a paste dict"""
        with sqlite3.connect(self.db_path) as cursor:
            row = cursor.execute(
                "SELECT * FROM pastes WHERE id = (?)", (paste_id,)
            ).fetchone()

        if row:
            _, paste_author, paste_content, filename, is_public = row
            return {
                "id": paste_id,
                "author": paste_author,
                "content": paste_content,
                "filename": filename,
                "public": is_public,
            }
        else:
            return None

    def add_paste(
        self,
        paste_author: str,
        paste_content: str,
        filename: str = None,
        is_public: bool = False,
    ) -> str:
        """Add a paste to the DB"""
        paste_id = str(uuid())
        if filename is None:
            filename = f"{paste_id}.txt"
        with sqlite3.connect(self.db_path) as cursor:
            cursor.execute(
                "INSERT INTO pastes(id, author, content, filename, public) VALUES (?,?,?,?,?)",
                (paste_id, paste_author, paste_content, filename, int(is_public)),
            )
        return paste_id, filename

    def delete_paste(self, paste_id: str) -> bool:
        """Drop a paste from the DB, returns a bool representing whether the paste was successfully deleted or not"""
        with sqlite3.connect(self.db_path) as cursor:
            row = cursor.execute(
                "SELECT * FROM pastes WHERE id = (?)", (paste_id,)
            ).fetchone()

            # Check that the paste exists before trying to delete it
            if row:
                cursor.execute(
                    "DELETE FROM pastes WHERE id = (?)", (paste_id,))
                return True

        return False

    def edit_paste(self, paste_id: str, new_content: str) -> bool:
        """Edit an existing paste in the DB, returns a bool representing whether the paste was successfully deleted or not"""
        with sqlite3.connect(self.db_path) as cursor:
            row = cursor.execute(
                "SELECT * FROM pastes WHERE id = (?)", (paste_id,)
            ).fetchone()

            # Check that the paste exists before trying to edit
            if row:
                cursor.execute(
                    "UPDATE pastes SET content = (?) WHERE id = (?)",
                    (
                        new_content,
                        paste_id,
                    ),
                )
                return True

        return False
