from flask import Flask
from flask_htpasswd import HtPasswdAuth
import logging
from logging import handlers

PASTE_DB_PATH = "paste_db.db"
FLASK_SECRET = "TODO"
HTPASSWD_PATH = "./.htpasswd"
LOG_BASE_FILENAME = "paste.log"

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.StreamHandler(),
        handlers.RotatingFileHandler(
            # 250 KB
            LOG_BASE_FILENAME,
            maxBytes=(25 * 10000),
            backupCount=7,
        ),
    ],
)


app = Flask(__name__)


app.config["FLASK_SECRET"] = FLASK_SECRET
app.config["FLASK_HTPASSWD_PATH"] = HTPASSWD_PATH
app.config["PASTE_DB_PATH"] = PASTE_DB_PATH

htpasswd = HtPasswdAuth(app)

from app import routes


logging.info("Starting...")
