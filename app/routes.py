"""
paste - routes.py
Defines routes for the paste web service
By Katelyn Hamer
"""

# from flask import render_template, request, Response, jsonify
import flask
from app import app, htpasswd
from .paste_db import PasteDatabase
import logging


PasteDB = PasteDatabase(app.config.get("PASTE_DB_PATH"))


@app.route("/token/get", methods=["GET"])
@htpasswd.required
def get_token(user):
    """Receive a token which can be supplied in future requests instead of htpasswd authentication"""
    logging.info(f"Handed out token to {user}")
    return flask.jsonify({"token": htpasswd.generate_token(user)})


@app.route("/paste/get", methods=["GET"])
def get_paste():
    """Get an existing paste"""
    if flask.request.json:
        paste_id = flask.request.json.get("id")
    else:
        paste_id = flask.request.args.get("id")

    if paste_id:
        if paste := PasteDB.get_paste(paste_id):
            if paste["public"]:
                # If the post is public, we don't need to do any authentication checks
                logging.info(f"Serving public paste {paste_id=}")
                if flask.request.args.get("raw"):
                    return paste["content"]
                else:
                    response = flask.jsonify(paste)
                    return response
            else:
                is_valid_auth, user = htpasswd.authenticate()
                if is_valid_auth and paste["author"] == user:
                    logging.info(f"Serving paste {paste_id=} to user {user}")
                    if flask.request.args.get("raw"):
                        return paste["content"]
                    else:
                        response = flask.jsonify(paste)
                        return response
                else:
                    response = flask.jsonify(
                        {"message": "You are not authorised to view this paste"}
                    )
                    return response, 403
        else:
            response = flask.jsonify(
                {"message": "Can't find a paste with this ID"})
            return response, 404
    else:
        response = flask.jsonify({"message": "ID field can't be empty"})
        return response, 400


@app.route("/paste/new", methods=["POST"])
@htpasswd.required
def create_paste(user):
    """Create a paste"""
    if flask.request.json:
        paste_content = flask.request.json.get("content")
        is_public = bool(flask.request.json.get("public"))
        filename = flask.request.json.get("filename", None)
    else:
        paste_content = flask.request.args.get("content")
        is_public = bool(flask.request.args.get("public"))
        filename = flask.request.args.get("filename", None)

    if paste_content:
        paste_id, filename = PasteDB.add_paste(
            user, paste_content, filename, is_public)
        logging.info(
            f"User {user} created paste {paste_id} (public: {is_public}, filename: {filename}, length: {len(paste_content)})"
        )
        response = flask.jsonify(
            {
                "id": paste_id,
                "url": f"{flask.request.url_root}paste/get?id={paste_id}&raw=1",
            }
        )
        return response
    else:
        response = flask.jsonify({"message": "Content field can't be empty"})
        return response, 400


@app.route("/paste/delete", methods=["DELETE"])
@htpasswd.required
def delete_paste(user):
    """Delete a paste"""
    if flask.request.json:
        paste_id = flask.request.json.get("id")
    else:
        paste_id = flask.request.args.get("id")

    if paste_id:
        if paste := PasteDB.get_paste(paste_id):
            if paste["author"] == user:
                PasteDB.delete_paste(paste_id)
                logging.info(f"User {user} deleted paste {paste_id}")
                response = flask.jsonify(
                    {"message": "The paste was successfully deleted"}
                )
                return response, 200
            else:
                response = flask.jsonify(
                    {"message": "You are not authorised to delete this paste"}
                )
                return response, 403
        else:
            response = flask.jsonify(
                {"message": "Can't find a paste with this ID"})
            return response, 404
    else:
        response = flask.jsonify({"message": "ID field can't be empty"})
        return response, 400


@app.route("/paste/edit", methods=["PUT"])
@htpasswd.required
def edit_paste(user):
    """Edit a paste"""
    if flask.request.json:
        paste_id = flask.request.json.get("id")
        content = flask.request.json.get("content")
    else:
        paste_id = flask.request.args.get("id")
        content = flask.request.args.get("content")

    if content:
        if paste_id:
            if paste := PasteDB.get_paste(paste_id):
                if paste["author"] == user:
                    PasteDB.edit_paste(paste_id, content)
                    logging.info(f"User {user} deleted paste {paste_id}")
                    response = flask.jsonify(
                        {"message": "The paste was successfully edited"}
                    )
                    return response, 200
                else:
                    response = flask.jsonify(
                        {"message": "You are not authorised to edit this paste"}
                    )
                    return response, 403
            else:
                response = flask.jsonify(
                    {"message": "Can't find a paste with this ID"})
                return response, 404
        else:
            response = flask.jsonify({"message": "ID field can't be empty"})
            return response, 400
    else:
        response = flask.jsonify({"message": "Content field can't be empty"})
        return response, 400


@app.route("/")
@app.route("/index")
@app.route("/paste")
def index():
    return flask.render_template("index.html")
