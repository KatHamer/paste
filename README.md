# Paste

Paste is a RESTful web service for creating and sharing snippets of text, similar to Hastebin.
The source code in this repository is hosted at https://paste.katelyn.dev.
## API Reference

### 🔒 `GET` **/token/get**
_Get an API token that you can use in subsequent requests_  
Response: JSON object containing a `token` field  

Example:  
- _In_: `{}`  
- _Out_: `{'token': 'AAaa...'}`  

### 🔓 `GET` **/paste/get**
_Retrieve an existing paste_  
Response: JSON object representing the appropriate paste (or content if raw=1)  
Request fields:  
- `id` (required) - Short ID of the paste  
- `raw` (optional) - If true, the paste body will be returned as plaintext instead of the paste JSON object, this is useful for sharing direct links to pastes.
  
Example:  
- _In_: `{'id': 'DQw4...'}`  
- _Out_: {'author': 'kat', 'content': '...', 'filename': 'DQw4....txt', 'id': 'DQw4...', 'public': 0}  

### 🔒 `POST` **/paste/new**
_Create a new paste_  
Response: JSON object containing two fields; `id`, the created paste's short ID and `url`, the created paste's URL  
Request fields:  
- `content` (required) - Paste body text  
- `filename` (optional) - Optional filename for the paste  
- `public` (optional) - False by default, if set to 1, the paste will be publicly accessible  

Example:  
- _In_: `{'content': 'Hello, world!', 'filename': 'hello.txt': 'public': 1}`  
- _Out_: `{'id': 'DQw4...': 'url': 'https://paste.katelyn.dev/get?id=DQw4...'}`

### 🔒 `DELETE` **/paste/delete**
_Delete an existing paste_  
Response: 200  
Request fields:  
- `id` (required) - Short ID of the paste  
  
Example:  
- _In_: `{'id': 'DQw4...'}`  
- _Out_: `{'message': 'Paste successfully deleted'}`  


### 🔒 `PUT` **/paste/edit**
Description: Edit the body text of an existing paste  
Response: 200  
Request fields:  
- `id` (required) - Short ID of the paste  
- `content` (required) - New body text  

Example:  
- _In_: `{'id': 'DQw4...', 'content': 'Hello, edited world!'}`  
- _Out_: `{'message': 'Paste successfully edited'}`  
### Authentication

All API calls that require authentication should either contain HTTP basic auth credentials OR provide an access token by providing the `Authorization` header in GitHub-style format (e.g. `token AAaa...`) or setting the `access_token` url encoded parameter. You can get a token by calling `/token/get` with your HTTP basic credentials.

### Supplying parameters

All parameters can be supplied via either url encoding or JSON, all responses are returned in JSON.  
  
## Deploying
- Clone this repo: `$ git clone https://gitlab.com/kathamer/paste && cd paste`
- Create a venv: `$ python3.10 -m venv venv`
- Activate venv: `$ source venv/bin/activate`
- Install dependencies: `$ pip install -r requirements.txt`
- Create and initialise the database:  
    `$ touch paste_db.db`  
    `$ python -i app/paste.py`   
    ```python  
    >>>  PasteDatabase('paste_db.db').init_db()  
    ```  
- Create and start an init service (a service file for systemd is included) that spawns `uwsgi` via the following command:
    `/path/to/venv/bin/uwsgi --ini app.ini`
- Configure your webserver to point to `uwsgi`'s socket
- Create a htpasswd file (using Apache's `htpasswd` utility is recommended) but the following command can be used instead:  
    `printf "USER:$(openssl passwd -crypt PASSWORD)\n" >> .htpasswd`
- Start the service!
